# Ansible Role: CarbonBlack

An Ansible role that deploys CarbonBlack on RedHat/OracleLinux 8. 

## Requirements

Repositories hosting the CarbonBlack tarballs are required to be in place, along with subsequent CBR servers, these will need to be deployed beforehand or available for this component to work. Iptable's rules are not provided, so these would need to be added additonally as well to harden the deployment, an example being. alternatively defined rules are provided in tasks directory.

`-A MGMT-Outbound -d {{ cbr_dom1 }} -p tcp -m tcp --dport 443 -m conntrack --ctstate NEW,ESTABLISHED -m comment --comment "Carbon Black for DOM1" -j ACCEPT
 71 COMMIT`

## Role Variables


Available variables, default values (see `defaults/main.yml`)
```
---
# Deployment variables for upgrading/installing new versions of CarbonBlack. 
# The only variable that will be required to change in most instances is the `cbvers` as it affects CHANGELOG/Archives

# URL
cbr_repo: "http://10.x.x.x/foo/carbonblack/{{ cbvers }}"

# CHANGELOG
usr_id: "ME"
action_completed: "Deployed: CarbonBlack"
cbvers: "v7.1.1.92158" # CHANGE this when upgrading


# CarbonBlack Archives
dl_site1_dom1: "CarbonBlackLinuxInstaller-{{ cbvers }}-SITE1ServerLinux.tar.gz" 
dl_site2_dom1: "CarbonBlackLinuxInstaller-{{ cbvers }}-SITE2ServerLinux.tar.gz" 
dl_site3_dom1: "CarbonBlackLinuxInstaller-{{ cbvers }}-SITE3ServerLinux.tar.gz" 
dl_dom2: "CarbonBlackLinuxInstaller-{{ cbvers }}-DOM2ServerLinux.tar.gz" 
dl_dom3: "CarbonBlackLinuxInstaller-{{ cbvers }}-DOM3ServerLinux.tar.gz" 

# CBR Servers
cbr_site1_dom1: "10.x.x.x"    
cbr_site2_dom1: "10.x.x.x" 
cbr_site3_dom1: "10.x.x.x"
cbr_dom2: "10.x.x.x"     
cbr_dom3: "10.x.x.x" 
```

## Dependencies

https://docs.vmware.com/en/VMware-Carbon-Black-EDR/7.6/cb-edr-scm-guide/GUID-40A345D4-2EB1-4720-A1FE-48F51AB7241A.html


## Deploy

`$ ansible-playbook deploy.yml -K`

```yaml
---
- hosts: all
  become: yes
  gather_facts: yes
  roles:
    - roles/ansible-carbonblack
  post_tasks:
   - name: Update /var/log/CHANGELOG
     ansible.builtin.include: roles/ansible-carbonblack/tasks/changelog_update.yml
```

## Licence

MIT/BSD
